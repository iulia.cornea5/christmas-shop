package product;

public abstract class Product {

    private String name;
    private double initialPrice;
    private double discount;
    private double commercialExcess;

    public Product(String name, double initialPrice, double commercialExcess) {
        this.name = name;
        this.initialPrice = initialPrice;
        this.commercialExcess = commercialExcess;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getInitialPrice() {
        return initialPrice;
    }

    public void setInitialPrice(double initialPrice) {
        this.initialPrice = initialPrice;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getCommercialExcess() {
        return commercialExcess;
    }
    public abstract double getSellingPrice();

    public double getProfit() {
        return getSellingPrice() - getInitialPrice();
    }
}
