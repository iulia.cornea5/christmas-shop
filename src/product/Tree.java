package product;

import product.enums.TreeType;

public class Tree extends Product {
    private TreeType treeType;
    private double transportationCost;

    public Tree(String name, TreeType treeType, double buyingPrice, double transportationCost) {
        super(name, buyingPrice, 0.3); //
        this.treeType = treeType;
        this.transportationCost = transportationCost;
    }

    public Tree(String name, TreeType treeType, double buyingPrice, double transportationCost, double commercialExcess) {
        super(name, buyingPrice, commercialExcess);
        this.treeType = treeType;
        this.transportationCost = transportationCost;
    }

    @Override
    public double getSellingPrice() {

        return getInitialPrice() + (1+ getCommercialExcess()) + transportationCost;
    }

    @Override
    public String toString() {
        return "Tree " + getName() + " " + this.treeType.toString().toLowerCase()
                + " bought for " + getInitialPrice()
                + " to be sold with " + getSellingPrice();
    }
}
