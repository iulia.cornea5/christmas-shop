package ex;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.SortedMap;

public class Exercises {

    public static void main(String[] args) {


        System.out.print("    843247239   \n\n\n");
        System.out.println("*");
        System.out.print("    843247239   \n\n\n".trim());
        System.out.println("*");


        ArrayList<String> words = new ArrayList<>();
        String input;
        Scanner scan = new Scanner(System.in);

        // CITIRE CUVINTE IN LISTA
        input = scan.nextLine(); // "STOP\n"
        while (!input.equals("STOP")) {
            words.add(input);
            input = scan.nextLine();
        }
        // CUVANTUL DE CAUTAT
        System.out.println("Dati cuvantul pe care vreti sa il cautati");
        String search = scan.nextLine();

        boolean found = words.contains(search);
        if (found) {
            System.out.println("Am\n gasit\n cuvantul " + search);
        } else {
            System.out.println("Cuvantul nu exista");
        }

        for (int i = 0; i < words.size(); i++) {
            String currentWord = words.get(i);
            if (currentWord.contains(search)) {
                System.out.println(currentWord + " contine " + search);
            }
        }



    }
}
